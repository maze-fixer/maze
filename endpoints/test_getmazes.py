from hypothesis import given
from hypothesis import strategies as st
from collections import namedtuple

from ..fixer.test_minfixer import maze
from .postmaze import CELL_FORMAT, GRIDSIZE_FORMAT

POSSIBLE_SOLUTION_STATUS = ["error: unsolveable", "error: multiple exits", "pending", "solved"]

@st.composite
def maze_from_db(draw):
    n_columns, n_rows, walls, entrance_column, entrance_row = draw(maze())
    solution_status = st.one_of(POSSIBLE_SOLUTION_STATUS)
    min_solution_status = draw(solution_status)
    max_solution_status = draw(solution_status)
    Maze = namedtuple("Maze", "id n_columns n_rows walls entrance_column entrance_row \
min_solution_status min_solution max_solution_status max_solution")
    return Maze(1, n_columns, n_rows, walls, entrance_column, entrance_row,
                min_solution_status, None, max_solution_status, None)

@given(st.iterables(maze_from_db()))
def test_render_get_mazes_result(mazelist):
    # note that we supply an ordinary iterable instead of a sqlalchemy CursorResult object
    render = render_get_mazes_result(mazelist)
    obj = json.loads(render)
    assert obj.keys() == ['id', 'gridsize', 'walls', 'entrance', 'min_solution_status',
                          'min_solution', 'max_solution_status', 'max_solution']
    assert isinstance(obj['id'], int)
    assert re.match(GRIDSIZE_FORMAT, obj['gridsize'])
    for wall in obj['walls']:
        assert re.match(CELL_FORMAT, wall)
    assert re.match(CELL_FORMAT, obj['entrance'])
    assert obj['min_solution_status'] in POSSIBLE_SOLUTION_STATUS
    assert obj['max_solution_status'] in POSSIBLE_SOLUTION_STATUS