from fastapi import APIRouter, HTTPException
from pydantic import BaseModel, Field
from sqlalchemy import select
import json
import string
from enum import Enum

from ..db import maze_table, execute
from .schemas import CellSet, SolutionStatus

router = APIRouter()


class StepsIn(str, Enum):
    min = "min"
    max = "max"


class SolutionOut(BaseModel):
    solution: CellSet = Field(..., example=["A0", "B0", "B1", "B2", "A2", "A3", "A4", "A5", "A6", "A7"])
    status: SolutionStatus = Field(..., example='solved')


@router.get("/maze/{mazeId}/solution", response_model=SolutionOut)
def get_solution(mazeId: int, steps: StepsIn):
    """Show the solutions and solution statuses of maze {mazeId}"""
    if steps == 'min':
        columns = (maze_table.c.min_solution, maze_table.c.min_solution_status)
    elif steps == 'max':
        columns = (maze_table.c.max_solution, maze_table.c.min_solution_status)
    query = select(*columns).where(maze_table.c.id == mazeId)

    solution = execute(query).one_or_none()
    if not solution:
        raise HTTPException(status_code=404, detail=f"maze {mazeId} could not be found")
    return _render_get_solution_result(*solution)

def _to_cellformat(col, row):
    return f"{string.ascii_uppercase[col]}{row}"

def _render_get_solution_result(solution, status):
    _to_cellformat = lambda c, r: string.ascii_uppercase[c] + str(r)
    solution = [_to_cellformat(c,r) for c,r in json.loads(solution or '[]')]
    # todo: native json type in DB
    return {
        'solution': solution,
        'status': status
    }
