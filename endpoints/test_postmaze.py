from hypothesis import given
from hypothesis import strategies as st
from sqlalchemy.sql.expression import Insert
import json

from .postmaze import _insert_values
from .postmaze import create_maze_query
from .postmaze import CELL_FORMAT, GRIDSIZE_FORMAT

cellpattern = st.from_regex(CELL_FORMAT)


@given(
    userid=st.integers(),
    gridsize=st.from_regex(GRIDSIZE_FORMAT),
    walls=st.lists(cellpattern),
    entrance=cellpattern)
def test_insert_values(userid, gridsize, walls, entrance):
    values = _insert_values(userid, gridsize, walls, entrance)
    assert isinstance(values, dict)
    assert list(values.keys()) == ['n_rows', 'n_columns', 'walls', 'entrance', 'owner']
    assert isinstance(values['n_rows'], int)
    assert isinstance(values['n_columns'], int)
    assert isinstance(values['walls'], str)
    json.loads(values['walls']) # check if valid json
    assert isinstance(values['entrance'], str)