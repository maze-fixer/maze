from hypothesis import strategies as st
from hypothesis import given

from .minfixer import solve_maze

@st.composite
def maze(draw):
    n_columns = draw(st.integers(1,10))
    n_rows = draw(st.integers(1,10))
    columnindex = st.integers(0, n_columns)
    rowindex = st.integers(0, n_rows)
    walls = draw(st.sets(
        st.tuples(columnindex, rowindex),
        max_size=n_rows * n_columns - 1)
    )
    while (entrance := draw(st.tuples(columnindex, rowindex))) in walls: continue
    return n_columns, n_rows, walls, entrance[0], entrance[1]


@given(maze())
def test_solve_maze(maze):
    status, solution = solve_maze(*maze)
    assert isinstance(status, str)
    assert isinstance(solution, list)
    # todo: flesh out these assertions
