from fastapi import APIRouter, Depends

from ..user.authentication import get_user

router = APIRouter(dependencies=[Depends(get_user)])

from .endpoints.getmazes import router as getmazes_router
router.include_router(getmazes_router)

from .endpoints.postmaze import router as postmaze_router
router.include_router(postmaze_router)

from .endpoints.getsolution import router as getsolution_router
router.include_router(getsolution_router)

